variable "name" {
  type = string
}

variable "resource_group_name" {
  type = string
}
variable "location" {
  type = string
}
variable "vnet_name" {
  type = string
}
variable "subnet_id" {
  type = string
}

variable "public_ip_id" {
  type = string
}
